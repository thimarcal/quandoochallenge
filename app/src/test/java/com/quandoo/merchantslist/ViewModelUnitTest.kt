package com.quandoo.merchantslist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.quandoo.merchantslist.data.model.Merchant
import com.quandoo.merchantslist.ui.merchants.MerchantsViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ViewModelUnitTest {
    lateinit var merchantsViewModel: MerchantsViewModel
    lateinit var firstMerchant: Merchant
    lateinit var secondMerchant: Merchant

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun create() {
        merchantsViewModel = mock(MerchantsViewModel::class.java)

        firstMerchant = mock(Merchant::class.java)
        secondMerchant = mock(Merchant::class.java)

        val merchantsLiveData = MutableLiveData<MutableList<Merchant>>()
        val singleMerchantLiveData = MutableLiveData<Merchant>()
        val list = mutableListOf<Merchant>()
        list.add(firstMerchant)
        list.add(secondMerchant)

        merchantsLiveData.value = list
        singleMerchantLiveData.value = firstMerchant

        `when`(merchantsViewModel.getMerchants()).thenReturn(merchantsLiveData as LiveData<List<Merchant>>)
        `when`(merchantsViewModel.getMerchant(ArgumentMatchers.eq(0))).thenReturn(firstMerchant)
    }

    @Test
    fun testGetError() {
        merchantsViewModel.getError()
        verify(merchantsViewModel).getError()
    }

    @Test
    fun testGetMerchants() {
        assert(merchantsViewModel.getMerchants().value?.size == 2)
    }

    @Test
    fun testGetSingleMerchant() {
        assert(merchantsViewModel.getMerchant(0).id == firstMerchant.id)
    }
}
