package com.quandoo.merchantslist.di.components

import com.quandoo.merchantslist.data.Repository
import com.quandoo.merchantslist.di.modules.DataModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(DataModule::class)])
interface DataComponent {

    fun inject(repository: Repository)

    @Component.Builder
    interface Builder {
        fun build(): DataComponent
        fun dataModule(dataModule: DataModule) : Builder
    }
}