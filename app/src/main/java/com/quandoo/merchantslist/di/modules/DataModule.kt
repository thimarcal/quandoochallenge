package com.quandoo.merchantslist.di.modules

import com.quandoo.merchantslist.data.retrofit.QuandooApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideRetrofit() = QuandooApi.create("http://api.quandoo.com/")

    @Provides
    @Singleton
    fun provideMerchantsApi(retrofit: Retrofit) = retrofit.create(QuandooApi::class.java)
}
