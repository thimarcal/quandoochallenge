package com.quandoo.merchantslist.data

import com.quandoo.merchantslist.data.model.Merchant
import com.quandoo.merchantslist.data.model.MerchantsResponse
import com.quandoo.merchantslist.data.retrofit.QuandooApi
import com.quandoo.merchantslist.di.components.DaggerDataComponent
import com.quandoo.merchantslist.di.components.DataComponent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class Repository private constructor(){

    @Inject
    lateinit var service : QuandooApi

    private var listeners = mutableListOf<DataReceivedListener>()

    private val component : DataComponent by lazy {
        DaggerDataComponent.builder().build()
    }

    init {
        component.inject(this)
    }

    fun registerListener(listener: DataReceivedListener) {
        listeners.add(listener)
    }

    fun unregisterListeners() {
        listeners.clear()
    }

    fun loadMerchants(offset: Int, limit: Int) {

        var merchants = service.getMerchants(offset, limit)

        merchants.enqueue(object: Callback<MerchantsResponse> {
            override fun onFailure(call: Call<MerchantsResponse>, t: Throwable) {
                listeners.forEach { it.onDataFailed(t)}
            }

            override fun onResponse(call: Call<MerchantsResponse>, response: Response<MerchantsResponse>) {
                val merchantsList = response.body()?.merchants

                if (merchantsList != null) {
                    listeners.forEach { it.onDataReceived(merchantsList) }
                }
            }
        })
    }

    companion object {
        @Volatile private var instance : Repository? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: Repository().also { instance = it }
            }
    }

    interface DataReceivedListener {
        fun onDataReceived(merchants: List<Merchant>)
        fun onDataFailed(t: Throwable)
    }
}