package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class Address(
    val city: String,
    val country: String,
    val number: String,
    val street: String,
    val zipcode: String
) : Serializable