package com.quandoo.merchantslist.data.retrofit

import com.quandoo.merchantslist.data.model.MerchantsResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface QuandooApi {
    @GET("v1/merchants")
    fun getMerchants(@Query("offset") offset: Int, @Query("limit") limit:Int) : Call<MerchantsResponse>

    companion object Factory{
        fun create(baseUrl: String) : Retrofit {
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .build()
        }
    }
}