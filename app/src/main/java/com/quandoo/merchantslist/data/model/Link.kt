package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class Link(
    val href: String,
    val method: String,
    val rel: String
) : Serializable