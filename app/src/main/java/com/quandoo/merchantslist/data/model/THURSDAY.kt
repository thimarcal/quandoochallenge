package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class THURSDAY(
    val end: String,
    val start: String
) : Serializable