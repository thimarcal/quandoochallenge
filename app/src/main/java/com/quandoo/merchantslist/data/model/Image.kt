package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class Image(
    val url: String
) : Serializable