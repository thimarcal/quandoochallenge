package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class StandardOpeningTimes(
    val FRIDAY: List<FRIDAY>,
    val MONDAY: List<MONDAY>,
    val SATURDAY: List<SATURDAY>,
    val THURSDAY: List<THURSDAY>,
    val TUESDAY: List<TUESDAY>,
    val WEDNESDAY: List<WEDNESDAY>
) : Serializable