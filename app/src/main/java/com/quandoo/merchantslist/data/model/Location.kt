package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class Location(
    val address: Address,
    val coordinates: Coordinates
) : Serializable