package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class OpeningTimes(
    val standardOpeningTimes: StandardOpeningTimes
) : Serializable