package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class Tag(
    val id: String,
    val name: String
) : Serializable