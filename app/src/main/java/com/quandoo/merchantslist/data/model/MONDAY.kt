package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class MONDAY(
    val end: String,
    val start: String
) : Serializable