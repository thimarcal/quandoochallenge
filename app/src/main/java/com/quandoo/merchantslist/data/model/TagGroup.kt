package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class TagGroup(
    val tags: List<Tag>,
    val type: String
) : Serializable