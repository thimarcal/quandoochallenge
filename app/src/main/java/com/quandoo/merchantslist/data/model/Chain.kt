package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class Chain(
    val id: Int,
    val name: String
) : Serializable