package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class FRIDAY(
    val end: String,
    val start: String
) : Serializable