package com.quandoo.merchantslist.data.model

data class MerchantsResponse(
    val limit: Int,
    val merchants: List<Merchant>,
    val offset: Int,
    val size: Int
)