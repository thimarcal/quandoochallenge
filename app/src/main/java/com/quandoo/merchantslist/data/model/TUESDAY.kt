package com.quandoo.merchantslist.data.model

import java.io.Serializable

data class TUESDAY(
    val end: String,
    val start: String
) : Serializable