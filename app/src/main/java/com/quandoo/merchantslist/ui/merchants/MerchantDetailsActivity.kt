package com.quandoo.merchantslist.ui.merchants

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.quandoo.merchantslist.R
import com.quandoo.merchantslist.data.model.Merchant
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_merchant_details.*


class MerchantDetailsActivity : AppCompatActivity() {

    val recyclerViewadapter = MerchantsImageAdapter(this)
    val linearLayoutManager = LinearLayoutManager(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merchant_details)

        val merchant = intent?.getSerializableExtra("Merchant") as Merchant

        ViewCompat.setTransitionName(profile_image, "${merchant.id}")

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        collapsingToolbarLayout.title = merchant.name
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsed_toolbar_title)
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expanded_toolbar_title)

        Picasso.get()
            .load(merchant.images[0].url)
            .fit().centerCrop()
            .into(profile_image, object : Callback {
                override fun onSuccess() {
                    supportStartPostponedEnterTransition()
                }

                override fun onError(e: Exception?) {
                    supportStartPostponedEnterTransition()
                }

            })
        merchant_address.text = "${merchant.location.address.street}, ${merchant.location.address.number}\n" +
                "${merchant.location.address.zipcode}"
        merchant_phone.text = merchant.phoneNumber
        rating.text = merchant.reviewScore

        imagesRecyclerView.layoutManager = linearLayoutManager
        imagesRecyclerView.adapter = recyclerViewadapter

        recyclerViewadapter.setImages(merchant.images)

        floatingActionButton.setOnClickListener {
            run {
                val gmmIntentUri = Uri.parse("geo:${merchant.location.coordinates.latitude}," +
                        "${merchant.location.coordinates.longitude}?q=${merchant.name}")
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                if (mapIntent.resolveActivity(packageManager) != null) {
                    startActivity(mapIntent)
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}
