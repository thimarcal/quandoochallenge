package com.quandoo.merchantslist.ui.merchants

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.quandoo.merchantslist.R
import com.quandoo.merchantslist.data.Repository
import kotlinx.android.synthetic.main.activity_merchants.*


class MerchantsActivity : AppCompatActivity(), MerchantsListAdapter.OnClickListener {

    val repository = Repository.getInstance()
    var factory = MerchantsViewModelFactory(repository)

    val recyclerViewAdapter = MerchantsListAdapter(this, this)
    val layoutManager = GridLayoutManager(this, 3)

    lateinit var lastMerchant : String
    var isLoading = false

    private val lastVisibleItemPosition: Int
        get() = layoutManager.findLastVisibleItemPosition()

    val viewModel by lazy {
        ViewModelProviders.of(this, factory).get(MerchantsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merchants)

        merchantsRecyclerView.adapter = recyclerViewAdapter
        merchantsRecyclerView.layoutManager = layoutManager

        viewModel.getError().observe(this, Observer { error ->
            Toast.makeText(this, getString(R.string.error_message), Toast.LENGTH_LONG).show()
        })

        isLoading = true
        progressBar.visibility = View.VISIBLE
        viewModel.getMerchants().observe(this, Observer { merchants ->
            run {
                isLoading = false
                progressBar.visibility = View.GONE

                recyclerViewAdapter.setMerchants(merchants)
            }
        })

        merchantsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!isLoading && recyclerView.layoutManager!!.itemCount == lastVisibleItemPosition + 1) {

                    isLoading = true
                    progressBar.visibility = View.VISIBLE
                    viewModel.loadMoreMerchants()
                }
            }
        })
    }

    override fun onClick(item: Int, pictureView: View) {
        val intent = Intent(this, MerchantDetailsActivity::class.java)
        val merchant = viewModel.getMerchant(item)
        intent.putExtra("Merchant", merchant)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            this,
            pictureView,
            ViewCompat.getTransitionName(pictureView) ?: "${merchant.id}"
        )
        startActivity(intent, options.toBundle())
    }
}
