package com.quandoo.merchantslist.ui.merchants

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.quandoo.merchantslist.data.Repository

class MerchantsViewModelFactory (private val repository: Repository) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MerchantsViewModel(repository) as T
    }
}