package com.quandoo.merchantslist.ui.merchants

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.quandoo.merchantslist.data.Repository
import com.quandoo.merchantslist.data.model.Merchant

class MerchantsViewModel (private val repository: Repository) : ViewModel(), Repository.DataReceivedListener {

    private var merchantsList = mutableListOf<Merchant>()
    private var merchantsLiveData = MutableLiveData<MutableList<Merchant>>()

    private var error = MutableLiveData<Throwable>()

    companion object {
        val DEFAULT_LIMIT = 30
        val INITIAL_LOAD = 120
        private var lastOffset = 0
    }

    init {
        repository.registerListener(this)
    }

    fun getError() = error as LiveData<Throwable>

    fun getMerchants() : LiveData<List<Merchant>> {
        if (merchantsList.isEmpty()) {
            for (offset in 0 until (INITIAL_LOAD/ DEFAULT_LIMIT)) {
                repository.loadMerchants(offset* DEFAULT_LIMIT, DEFAULT_LIMIT)
            }
        }

        return merchantsLiveData as LiveData<List<Merchant>>
    }

    fun getMerchant(position: Int) = merchantsList[position]

    fun loadMoreMerchants() {
        lastOffset += DEFAULT_LIMIT
        repository.loadMerchants(lastOffset, DEFAULT_LIMIT)
    }

    override fun onDataReceived(merchants: List<Merchant>) {
        merchantsList.addAll(merchants)
        merchantsLiveData.value = merchantsList
    }

    override fun onDataFailed(t: Throwable) {
        error.value = t
    }

}