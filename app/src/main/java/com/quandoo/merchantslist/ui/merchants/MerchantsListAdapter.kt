package com.quandoo.merchantslist.ui.merchants

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.quandoo.merchantslist.R
import com.quandoo.merchantslist.data.model.Merchant
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.merchant_item.view.*

class MerchantsListAdapter(private val context: Context, private val clickListener: OnClickListener)
                                        : RecyclerView.Adapter<MerchantsListAdapter.MerchantsListViewHolder>() {
    private var merchants = mutableListOf<Merchant>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MerchantsListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.merchant_item, parent, false)

        return MerchantsListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return merchants.size
    }

    override fun onBindViewHolder(holder: MerchantsListViewHolder, position: Int) {
        holder.name.text = merchants[position].name
        holder.view.tag = position
        Picasso.get()
            .load(merchants[position].images[0].url)
            .fit().centerCrop()
            .placeholder(R.drawable.image_placeholder)
            .into(holder.picture)

        ViewCompat.setTransitionName(holder.picture, "${merchants[position].id}")

        holder.itemView.setOnClickListener {
            clickListener.onClick(position, holder.picture)
        }
    }

    fun setMerchants(merchantList: List<Merchant>) {
        merchants = merchantList as MutableList<Merchant>
        notifyDataSetChanged()
    }

    class MerchantsListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view = itemView
        val picture = itemView.merchantPicture
        val name = itemView.merchantName
    }

    interface OnClickListener {
        fun onClick(item : Int, pictureView: View)
    }
}