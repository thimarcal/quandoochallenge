package com.quandoo.merchantslist.ui.merchants

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.quandoo.merchantslist.R
import com.quandoo.merchantslist.data.model.Image
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.photo_item.view.*

class MerchantsImageAdapter(private val context: Context)
                                        : RecyclerView.Adapter<MerchantsImageAdapter.MerchantsImageViewHolder>() {
    private var images = mutableListOf<Image>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MerchantsImageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.photo_item, parent, false)

        return MerchantsImageViewHolder(view)
    }

    override fun getItemCount(): Int {
        return images.size
    }

    override fun onBindViewHolder(holder: MerchantsImageViewHolder, position: Int) {
        Picasso.get()
            .load(images[position].url)
            .fit().centerCrop()
            .placeholder(R.drawable.image_placeholder)
            .into(holder.picture)
    }

    fun setImages(imagesList: List<Image>) {
        images = imagesList as MutableList<Image>
        notifyDataSetChanged()
    }

    class MerchantsImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val picture = itemView.picture


    }
}