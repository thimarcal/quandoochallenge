Quandoo Android Codign Challenge

Candidate: Thiago Gomes Mar�al Pereira (thiago.gm.pereira@gmail.com)

This app follows the requirements from https://gitlab.com/quandoo-recruitment/android-engineer/tree/master

Main points:
- Used MVVM, DI (Dagger2), Retrofit and Kotlin

Also added an intent to open the map to the restaurant.

Didn't implement an offline mode, because of lack of spare time.
Would probably do so, using Room Database. I have used it before a few times, and can provide links if you wish.

Other than that, I would also improve the connectivity treatment. Handle connectivity changes, and correctly notifying the user (maybe using a snackbar with a retry action), instead of just using the poor Toast I'm using when something happens when trying and failing to fetch data from the API.

Implemented just a few tests with Mokito, but would also use Espresso to validate other items, for example the Details screen.

Layout could also be improved, to better colors, and with more information.

